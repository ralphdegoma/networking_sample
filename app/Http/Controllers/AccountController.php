<?php

namespace App\Http\Controllers;

use App\Order;
use App\Product;
use App\Referral;
use App\ReferralMember;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class AccountController extends Controller
{
    public function editAccount(Request $request){

        $validator = Validator::make($request->all(), [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255',],
            'phone' => ['required'],
            'address' => ['required'],
        ]);

        if ($validator->fails()){
            return response()->json(['status' => 'error', 'errors' => $validator->errors()->all()], 422);
        }
        if($request->hasfile('avatar'))
        {
            $avatarImgPath = Storage::disk('uploads')->put('avatar', $request->avatar);
            $avatarImagePath = url('/').'/uploads/'.$avatarImgPath;
        }

        $user = User::find($request->user_id);
        $user->email = $request->email;
        $user->name = $request->name;
        $user->address = $request->address;
        $user->avatar =  $request->hasfile('avatar') ? $avatarImagePath : null;
        $user->phone = $request->phone;
        $user->save();

        return response()->json(['status' => 'success', 'user' => $user ]);

    }

    public function changePassword(Request $request){

        $validator = Validator::make($request->all(), [
            'old_password' => ['required'],
            'new_password' => ['required', 'max:255'],
            'new_password_confirm' => ['required'],
        ]);

        if ($validator->fails()){
            return response()->json(['status' => 'error', 'errors' => $validator->errors()->all()], 422);
        }

        if(!Hash::check($request->old_password, auth()->user()->password)){
            return response()->json(['status' => 'error', 'errors' => ["Old Password is incurrect."]], 422);
        }

        $user = User::find(auth()->user()->id)->update(['password'=> Hash::make($request->new_password)]);

        return response()->json(['status' => 'success', 'user' => $user ]);
    }

    public function generateReferalCode(Request $request){
        
        $referral = Referral::where('user_id', Auth::id())->first();

        if($referral){
            return response()->json(['status' => 'error', 'error' => "You have already a referal code"], 422);
        }

        $data = [];
        $data['order'] = Order::where('user_id', Auth::id())->where('status', 'complete')->first();
        $data['referral'] = Referral::create([
                'code' => $this->random_strings(2).'_'.rand(10000,99999),
                'user_id' => Auth::id()
            ]);

        return response()->json($data, 200);
    }

    public function getGeneratedReferralCode(){
        $data = [];
        $data['order'] = Order::where('user_id', Auth::id())->where('status', 'complete')->first();
        $data['referral'] =  Referral::where('user_id', Auth::id())->first();

        return response()->json(['status' => 'success', 'referral' => $data ], 200);
       
    }

    public function random_strings($length_of_string) 
    { 
    
        // String of all alphanumeric character 
        $str_result = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz'; 
    
        // Shufle the $str_result and returns substring 
        // of specified length 
        return substr(str_shuffle($str_result),  
                        0, $length_of_string); 
    } 

    public function analyticsCount() {
        $users = User::where('user_type', null)->get();
        $products = Product::all();
        $orders = Order::all();
        $order_pending = Order::where('status', 'pending')->get();
        $order_process = Order::where('status', 'process')->get();
        $order_complete = Order::where('status', 'complete')->get();
        $order_reject = Order::where('status', 'reject')->get();
        $order_refund = Order::where('status', 'refund')->get();
        return response()->json([
            'users_count' => count($users),
            'products_count' => count($products),
            'orders_count' => count($orders),
            'order_pending_count' => count($order_pending),
            'order_process_count' => count($order_process),
            'order_complete_count' => count($order_complete),
            'order_reject_count' => count($order_reject),
            'order_refund_count' => count($order_refund),
        ], 200);
    }

    public function network(){
        $referral = Referral::where('user_id', Auth::id())->first();
        if($referral){

            $referral_level_1 = ReferralMember::where('referral_id', $referral->id)->where('order', 1)->get();
            $referral_level_2 = ReferralMember::where('referral_id', $referral->id)->where('order', 2)->get();
            $referral_level_3 = ReferralMember::where('referral_id', $referral->id)->where('order', 3)->get();
            $referral_level_4 = ReferralMember::where('referral_id', $referral->id)->where('order', 4)->get();
            $referral_level_5= ReferralMember::where('referral_id', $referral->id)->where('order', 5)->get();
            $referrals= ReferralMember::where('referral_id', $referral->id)->get();

            // level-1
            $referralMembersLvl1 = ReferralMember::where('referral_id', $referral->id)->where('order', 1)->with('user:id,name,avatar')->get(['user_id', 'referral_id', 'created_at']);
            if(count($referralMembersLvl1) > 0){
                foreach ($referralMembersLvl1 as $lvl1) {
                    // level-2
                    $lvl1->label = 'Level 1 - '.$lvl1->user->name.' - '. date('F j, Y', strtotime($lvl1->created_at));
                    $lvl1->name = $lvl1->user->name;
                    $lvl1->level = 1;
                    $lvl1->date = date('F j, Y', strtotime($lvl1->created_at));
                    $lvl1->avatar = $lvl1->user->avatar;

                    $referralLvl1 = Referral::where('user_id',  $lvl1->user_id)->first();
                    if($referralLvl1){
                        $referralMembersLvl2 = ReferralMember::where('referral_id', $referralLvl1->id)->where('order', 1)->with('user:id,name,avatar')->get(['user_id', 'referral_id', 'created_at']);
                        $lvl1->children = $referralMembersLvl2;

                        if(count($lvl1->children) > 0){
                            foreach ($lvl1->children as $lvl2) {
                                 // level-2
                                $lvl2->label = 'Level 2 - '.$lvl2->user->name.' - '. date('F j, Y', strtotime($lvl2->created_at));
                                $lvl2->name = $lvl2->user->name;
                                $lvl2->level = 2;
                                $lvl2->date = date('F j, Y', strtotime($lvl2->created_at));
                                $lvl2->avatar = $lvl2->user->avatar;
                                
                                $referralLvl2 = Referral::where('user_id',  $lvl2->user_id)->first();
                                if($referralLvl2){
                                    $referralMembersLvl3 = ReferralMember::where('referral_id', $referralLvl2->id)->where('order', 1)->with('user:id,name,avatar')->get(['user_id', 'referral_id', 'created_at']);
                                    $lvl2->children = $referralMembersLvl3;

                                    if(count($lvl2->children) > 0){
                                        foreach ($lvl2->children as $lvl3) {
                                             // level-2
                                            $lvl3->label = 'Level 3 - '.$lvl3->user->name.' - '. date('F j, Y', strtotime($lvl3->created_at));
                                            $lvl3->name = $lvl3->user->name;
                                            $lvl3->level = 3;
                                            $lvl3->date = date('F j, Y', strtotime($lvl3->created_at));
                                            $lvl3->avatar = $lvl3->user->avatar;
                                            
                                            $referralLvl3 = Referral::where('user_id',  $lvl3->user_id)->first();
                                            if($referralLvl3){
                                                $referralMembersLvl4 = ReferralMember::where('referral_id', $referralLvl3->id)->where('order', 1)->with('user:id,name,avatar')->get(['user_id', 'referral_id', 'created_at']);
                                                $lvl3->children = $referralMembersLvl4;
                    
                                                if(count($lvl3->children) > 0){
                                                    foreach ($lvl3->children as $lvl4) {
                                                         // level-2
                                                        $lvl4->label = 'Level 4 - '.$lvl4->user->name.' - '. date('F j, Y', strtotime($lvl4->created_at));
                                                        $lvl4->name = $lvl4->user->name;
                                                        $lvl4->level = 4;
                                                        $lvl4->date = date('F j, Y', strtotime($lvl4->created_at));
                                                        $lvl4->avatar = $lvl4->user->avatar;
                                                        
                                                        $referralLvl4 = Referral::where('user_id',  $lvl4->user_id)->first();
                                                        if($referralLvl4){
                                                            $referralMembersLvl5 = ReferralMember::where('referral_id', $referralLvl4->id)->where('order', 1)->with('user:id,name,avatar')->get(['user_id', 'referral_id', 'created_at']);
                                                            $lvl4->children = $referralMembersLvl5;

                                                            foreach ($lvl4->children as $value) {
                                                                $value->label = 'Level 5 - '.$value->user->name.' - '. date('F j, Y', strtotime($value->created_at));
                                                                $value->name = $value->user->name;
                                                                $value->level = 5;
                                                                $value->date = date('F j, Y', strtotime($value->created_at));
                                                                $value->avatar = $value->user->avatar;
                                                            }
                                                        }
                                                       
                                                    }
                                                }
                                            }
                                           
                                        }
                                    }
                                }
                              
    
                               
                            }
                        }
                    }
                  

                    
                }
            }

            return response()->json([
                'referral_level_1' => count($referral_level_1),
                'referral_level_2' => count($referral_level_2),
                'referral_level_3' => count($referral_level_3),
                'referral_level_4' => count($referral_level_4),
                'referral_level_5' => count($referral_level_5),
                'referrals' => count($referrals),
                'referral_tree' => $referralMembersLvl1
            ], 200);
       
        }

        return null;
    }

}
